﻿using System;
namespace Xrx.Extensions
{
    public static class ObjectExtensions
    {
        public static bool IsNull(this object self) =>
            self == null;
    }
}
