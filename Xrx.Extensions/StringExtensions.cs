﻿using System;
using System.Text.RegularExpressions;

namespace Xrx.Extensions
{
    public static class StringExtensions
    {
        public static bool IsEmail(this string self)
        {
            return Regex.IsMatch(self, @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", RegexOptions.IgnoreCase);
        }
        public static bool IsNotNullEmptyOrWhitespace(this string self)
        {
            return string.IsNullOrWhiteSpace(self) || self == string.Empty;
        }
        public static bool ContainsLetterNumberSymbol(this string self)
        {
            bool containsSymbol = self.ContainsSymbol();
            bool containsLetter = Regex.IsMatch(self, "[a-z]", RegexOptions.IgnoreCase);
            bool containsNumber = Regex.IsMatch(self, "[0-9]");
            return containsLetter && containsNumber && containsSymbol;
        }
        public static bool ContainsSymbol(this string self)
        {
            return Regex.IsMatch(self, "[!@#$%^&*()-_=+~{}|:;',\"./<>?]");
        }
        public static bool IsInt(this string self)
        {
            return int.TryParse(self, out int result);
        }

    }
}
