﻿using System;
namespace Xrx.Extensions
{
    public static class IntExtensions
    {
        public static bool IsEven(this int self)
        {
            return self % 2 == 0;
        }
    }
}
